import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiWeatherService {
  constructor(private http: HttpClient) {}


  // Service to get data information
    getData(idName: string):Observable<any> {
    try {
      const response =   this.http.get(
        `https://api.weather.gov/gridpoints/${idName}/31,80/forecast`
      );

      return response;
    } catch (error) {
      console.error('Error fetching data', error);
      throw error;
    }
  }
}
