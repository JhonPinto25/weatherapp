import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {

  idNameSelected:string = 'TOP';

  rutas = [
    {
      name: 'District of Columbia Forecast ( LWX )',
      path: '/weather/LWX',
      description: "Washington, D.C., formally the District of Columbia and commonly called Washington or D.C., is the capital city and federal district of the United States."
    },
    {
      name: 'Kansas Forecast ( TOP )',
      path: '/weather/TOP',
      description: "Kansas is a landlocked state in the Midwestern region of the United States. It borders Nebraska to the north; Missouri to the east; Oklahoma to the south; and Colorado to the west. Kansas is named after the Kansas River, in turn named after the Kansa people. Its capital is Topeka, and its most populous city is Wichita, however the largest urban area is the bi-state Kansas City, MO–KS metropolitan area."
    }
  ]


}
