import { Component, OnInit } from '@angular/core';
import { ApiWeatherService } from '../../services/api-weather.service';
import { ActivatedRoute } from '@angular/router';
import { Chart } from 'chart.js/auto';
import moment from 'moment';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrl: './weather.component.css',
})
export class WeatherComponent implements OnInit {
  weatherData: any = '';
  idSelected: any = '';
  arrayNames: string[] = [];
  arraytemperatures: number[] = [];
  arrayStartTime: string[] = [];
  arrayEndTime: string[] = [];
  arrayDetailsForecast: string[] = [];
  arrayWindDirecction: string[] = [];
  arrayWindSpeed: string[] = [];

  constructor(
    private serviceData: ApiWeatherService,
    private route: ActivatedRoute
  ) {
    this.idSelected = this.route.snapshot.paramMap.get('id');


    if (this.idSelected == null) {
      return;
    }
  }

  ngOnInit() {
    this.getData();
  }

  // Function where I get the data in the service.
  getData() {
    try {
      if (this.idSelected == null) {
        return;
      }
      this.serviceData.getData(this.idSelected).subscribe((response) => {
        this.weatherData = response;
        console.log(this.weatherData);
        this.mapData();
        this.renderChart();
      });
    } catch (error) {
      console.error('Error loading data', error);
    }
  }


// Function where I get the detailed data.
  mapData() {
    for (
      let index = 0;
      index < this.weatherData.properties.periods.length;
      index++
    ) {
      this.arrayNames.push(this.weatherData.properties.periods[index].name);
      this.arraytemperatures.push(
        this.weatherData.properties.periods[index].temperature
      );
      this.arrayDetailsForecast.push(
        this.weatherData.properties.periods[index].detailedForecast
      );
      this.arrayStartTime.push(
        moment
          .utc(this.weatherData.properties.periods[index].startTime)
          .format('MM/DD/YY HH:mm:ss')
      );
      this.arrayEndTime.push(
        moment
          .utc(this.weatherData.properties.periods[index].endTime)
          .format('MM/DD/YY HH:mm:ss')
      );
      this.arrayWindDirecction.push(
        this.weatherData.properties.periods[index].arrayWindDirecction
      );
      this.arrayWindSpeed.push(
        this.weatherData.properties.periods[index].arrayWindSpeed
      );
    }
  }

  // Rendering the chart using chart.js
  renderChart() {
    new Chart(
      'myChart',

      {
        type: 'line',
        data: {
          labels: this.arrayNames,

          datasets: [
            {
              label: 'Temperature forecast in Fahrenheit',
              data: this.arraytemperatures,
              borderWidth: 1,
            },
          ],
        },
        options: {
          responsive: true,

          scales: {
            y: {
              beginAtZero: true,
            },
          },

          plugins: {
            tooltip: {
              titleFont: {
                size: 14,
              },

              bodyFont: {
                size: 10,
              },

              callbacks: {
                title: function (tooltipItems) {
                  return (
                    tooltipItems[0].label + ': ' + tooltipItems[0].raw + '°F'
                  );
                },

                label: (tooltipItems) => {
                  return (
                    'Detailed Forecast: ' +
                    this.arrayDetailsForecast[tooltipItems.dataIndex]
                  );
                },

                beforeLabel: (tooltipItems) => {
                  return (
                    'Start Time: ' +
                    this.arrayStartTime[tooltipItems.dataIndex] +
                    '\n' +
                    'End Time: ' +
                    this.arrayEndTime[tooltipItems.dataIndex]
                  );
                },
              },
            },
          },
        },
      }
    );
  }
}
